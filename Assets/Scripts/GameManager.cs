﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;

public class GameManager : MonoBehaviour {

    public RuneManager runeManager;
    public DataPool imageDataPool;

    public List<Rune> listRuneHint;

    private List<int> answer   = new List<int>();
    private List<int> question = new List<int>();

    // Use this for initialization
    void Start () {
        if (listRuneHint.Count != 3)
            throw new System.Exception("List Rune Hint must be 3 items");
        if (runeManager == null || imageDataPool == null)
            throw new System.Exception("All field must be filled");

        FillAnswer();
        GenerateQuestion();
        RenderRuneHint();
        runeManager.SetRuneAnswerValues(question.ToArray());
    }
	
	// Update is called once per frame
	void Update () {

    }

    private void RenderRuneHint() {
        for (int i = 0; i < listRuneHint.Count; i++) {
            listRuneHint[i].SetValue(answer[i]);
        }
    }

    private void FillAnswer() {
        answer.Clear();
        int imageDataPoolSize = imageDataPool.GetSize();

        while (answer.Count < 3) {
            int index = Random.Range(0, imageDataPoolSize - 1);
            bool found = false;
            for (int i = 0; i < answer.Count; i++) {
                int element = answer[i];
                if (index == element) {
                    found = true;
                    break;
                }
            }
            if (!found)
                answer.Add(index);
        }
    }

    private void GenerateQuestion() {
        question = new List<int>(answer);
        while (true) {
            for (int i = 0; i < 5; i++) {
                int firstIndex = Random.Range(0, question.Count);
                int secondIndex = Random.Range(0, question.Count);

                int temp = question[firstIndex];
                question[firstIndex] = question[secondIndex];
                question[secondIndex] = temp;
            }

            bool same = true;
            for (int i = 0; i < answer.Count; i++) {
                if (answer[i] != question[i]) {
                    same = false;
                    break;
                }
            }

            if (!same)
                break;
        }
    }

    // Called when the answer is correct
    // So generate another puzzle
    private void Correct() {
        FillAnswer();
        GenerateQuestion();
        RenderRuneHint();
        runeManager.SetRuneAnswerValues(question.ToArray());
    }

    public void CheckAnswer(List<int> tempAnswer) {
        for (int i = 0; i < tempAnswer.Count; i++) {
            if (tempAnswer[i] != answer[i])
                return;
        }

        Correct();
    }
}
