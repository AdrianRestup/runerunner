﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Rune : MonoBehaviour {
    public int runeValue;
    public bool isInteractable = false;

    private RectTransform rectTransform;
    private Image imageContainer;

    private float lerp;
    private bool isShowing;
    private float initPosY;
    public bool isSelected;
    public bool isSwaping;

    private DataPool dataPool;

    void Awake () {
        dataPool = GameObject.Find("DataPool").GetComponent<DataPool>();
        rectTransform = GetComponent<RectTransform>();
        imageContainer = GetComponent<Image>();
    }

	// Use this for initialization
	void Start () {
        lerp = 0f;
        isShowing = false;
        isSwaping = false;
        isSelected = false;

        initPosY = rectTransform.anchoredPosition.y;
        imageContainer.color = new Color(1f, 1f, 1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
	    if(isShowing) {
            if(lerp >= 1f) {
                isShowing = false;
            } else {
                lerp += Time.deltaTime * 5f;

                float alpha = Mathfx.Lerp(0f, 1f, lerp);
                float posY = (isInteractable) ? Mathfx.Lerp(initPosY - 100f, initPosY, lerp) : Mathfx.Lerp(initPosY + 100f, initPosY, lerp);

                rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, posY);
                imageContainer.color = new Color(1f, 1f, 1f, alpha);
            }
        }

        if(isSwaping) {
            if (lerp >= 1f) {
                isSwaping = false;
                isSelected = false;
            } else {
                lerp += Time.deltaTime * 5f;

                float scale = Mathfx.Berp(0f, 1f, lerp);

                rectTransform.localScale = new Vector3(scale, scale, 1f);
            }
        }
	}

    public void SetValue(int valueIndex) {
        runeValue = valueIndex;
        imageContainer.sprite = dataPool.GetRuneImage(valueIndex);
        rectTransform.localScale = Vector3.one;
        lerp = 0f;
        isShowing = true;
    }

    public void Selected() {
        if (isSelected) {
            rectTransform.localScale = Vector3.one;
        } else {
            rectTransform.localScale = new Vector3(0.8f, 0.8f, 1f);
        }

        isSelected = !isSelected;
    }

    public void Swap(int valueIndex) {
        runeValue = valueIndex;
        rectTransform.localScale = Vector3.one;
        imageContainer.sprite = dataPool.GetRuneImage(valueIndex);
        lerp = 0f;
        isSwaping = true;
    }

    public void Reset() {
        isSelected = false;
        rectTransform.localScale = Vector3.one;
    }
}
