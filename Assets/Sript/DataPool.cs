﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataPool : MonoBehaviour {
    public List<Sprite> listImages = new List<Sprite>();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public Sprite GetRuneImage(int index) {
        return listImages[index];
    }

    public int GetSize() {
        return listImages.Count;
    }
}
