﻿using UnityEngine;
using System.Collections;

public class CLog {
    private static bool showLog = true;

    public static void Log(string message) {
        if(showLog)
            Debug.Log(message);
    }
}
