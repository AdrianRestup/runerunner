﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {
    public GameObject playerCharacter;

    [Header("Position")]
    public float initPosY;
    public float initPosX;
    public float toPosY;

    private float lerp;
    private bool isShowing;
    private bool isMoving;
    private float speed;

	// Use this for initialization
	void Start () {
        transform.position = new Vector3(initPosX, initPosY, 1f);
	}
	
	// Update is called once per frame
	void Update () {
	    if(isShowing) {
            if(lerp >= 1f) {
                lerp = 0f;
                isShowing = false;
                isMoving = true;
            } else {
                lerp += Time.deltaTime * 5f;

                float posY = Mathfx.Lerp(initPosY, toPosY, lerp);

                transform.position = new Vector3(initPosX, posY, 1f);
            }
        }

        if(isMoving) {
            if(lerp >= 1f) {
                isMoving = false;
            } else {
                lerp += Time.deltaTime * speed;

                float posX = Mathfx.Lerp(initPosX, playerCharacter.transform.position.x, lerp);

                transform.position = new Vector3(posX, toPosY, 1f);
            }
        }
	}

    // Set speed based on difficulty?
    public void Show(float speed) {
        lerp = 0f;
        isShowing = true;
        isMoving = false;
        this.speed = speed;
    }
}
