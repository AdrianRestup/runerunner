﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RuneManager : MonoBehaviour {
    public List<Rune> listRuneAnswer;

    public List<Rune> storeAnswer = new List<Rune>(); // temporary store rune value to be swapped

    public GameManager gameManager;

    [Header("DUMMY")]
    public Obstacle rock;

    private bool checkAnswer;

	// Use this for initialization
	void Start () {
        if (gameManager == null)
            throw new System.Exception("All field must be filled");
        if (listRuneAnswer.Count != 3)
            throw new System.Exception("List Rune Answer must be 3 items");

        listRuneAnswer[0].GetComponent<Button>().onClick.AddListener(() => { SwapRune(listRuneAnswer[0]); });
        listRuneAnswer[1].GetComponent<Button>().onClick.AddListener(() => { SwapRune(listRuneAnswer[1]); });
        listRuneAnswer[2].GetComponent<Button>().onClick.AddListener(() => { SwapRune(listRuneAnswer[2]); });

        checkAnswer = false;
    }
	
	// Update is called once per frame
	void LateUpdate () {
	    if(checkAnswer && RuneAvailable()) {
            // TODO check answer here from listRuneAnswer runeValue
            List<int> tempAnswer = new List<int>();
            for (int i = 0; i < listRuneAnswer.Count; i++) {
                tempAnswer.Add(listRuneAnswer[i].runeValue);
            }
            gameManager.CheckAnswer(tempAnswer);
            
            // Clear list
            storeAnswer.Clear();
            checkAnswer = false;
        } 
	}

    // Masukin randoman list jawaban dari game manager
    public void SetRuneAnswerValues(int[] values) {
        for(int i = 0; i < listRuneAnswer.Count; i++) {
            listRuneAnswer[i].SetValue(values[i]);
        }
    }

    // DUMMY
    public void SetRuneAnswerValues() {
        List<int> values = new List<int>();
        values.Add(Random.Range(0, 10));
        values.Add(Random.Range(0, 10));
        values.Add(Random.Range(0, 10));

        for (int i = 0; i < listRuneAnswer.Count; i++) {
            listRuneAnswer[i].SetValue(values[i]);
        }

        rock.Show(1f); // DUMMY yah
    }

    public void SwapRune(Rune rune) {
        if (rune.isSelected) {
            rune.Selected();
            storeAnswer.Remove(rune);
        } else {
            rune.Selected();
            storeAnswer.Add(rune);

            if (storeAnswer.Count == 2) {
                CLog.Log("Check");
                int tempValue = storeAnswer[1].runeValue;

                storeAnswer[1].Swap(storeAnswer[0].runeValue);
                storeAnswer[0].Swap(tempValue);
                
                checkAnswer = true;
            }
        }
    }

    private bool RuneAvailable() {
        bool available = true;

        foreach(Rune rune in storeAnswer) {
            if (rune.isSwaping)
                return false;
        }

        return available;
    }
}
